﻿---
documentclass: article
title: |
  | ![LOGO ILO](ilo.png){ height=2cm }
  | Position Paper
subtitle: |
  | Recognizing Opportunities for the Reestablishment of Just and Progressive Working Conditions in post-war Angola
author:
  - Simone Müller
  - Marty Oehme
  - Jonas Walter
titlepagetop: |
  | UNIVERSITÄT LEIPZIG
  | History of Global Development
  | WS 2019/2020
  | Dr. Steffi Marung
titlepagebottom: |
  | Simone Müller
  | Marty Oehme
  | Jonas Walter
date: February 5, 1988
papersize: A4
geometry:
  - left=2.2cm
  - right=3.5cm
  - top=2.5cm
  - bottom=2.5cm
indent: true
linestretch: 1.45
fontfamily: lmodern
fontsize: 12
lang: en
bibliography: /home/marty/documents/library/academia/academia.bib
csl: /home/marty/documents/library/academia/CMS-fullnote-bibliography-ibid.csl
fancyheader-settings:
  - \fancyhead[L]{Position Paper --- Angolan Reconstruction Efforts}
  - \fancyhead[R]{International Labour Organization}
output:
  pdf_document:
    latex_engine: xelatex
---

After years of civil war the International Community has come together in New York to find a solution for the transition of post-war Angola into a peaceful nation. Those years of fighting have left the country and its people in a devastating situation.

Armed violence and civil disorder led to an over-exploitation of resources, forced labour and child labour in essential industries such as the diamond and oil sector, rural exodus, internal migration and displacement of farmers, and consequently to poverty and famines among Angolan citizens.

The country of Angola does not deserve such continued conditions of deprivation and struggle. It is a region blessed with an abundance of natural and human resources as well as ample lands suitable for agricultural use by the unified people of Angola.

As alarming as the current circumstances appear to be, we are confident that, with the cooperation and generous help of the International Community, we shall be able to provide a foundation for stability, for prosperity, and for a common progress toward the satisfying fulfillment of Angola's ambitions.

We, as International Labour Organization, herewith address the International Community to find resolutions for the transition to a democratic, stable society with a promotion of social justice and decent labour for all.

## Problem Area: Consolidating Wage Labour

### Challenges

* Since the 1960s wage labour contract work has continually decreased
* Little to no unionization currently exists within Angola's economic landscape
* Increasing inflation rates drag down real wages to necessitate informal labour
* No work unionization prevents cohesive collective bargaining possibilities

### Objectives

Supporting **labour participation in social dialogue** through the **reintegration of displaced persons**, by providing **vocational and targeted training**, **administrative assistance**, and putting an emphasis on **strengthening the existing weak unionization possibilities for both individual workers and collective organizations**.
Enabling and supporting just and effective opportunities of collective bargaining through this strengthening of the workforce.

## Problem Area: Enabling Agrarian Sector

### Challenges

* Residual land mines complicate work on fields and pose imminent dangers to the local workers
* Increasing collapse of the agrarian sector due to lack of capital investments
* Leading to underproduction of agrarian goods; threatening famine and wide-spread poverty
* Managing a currently dysfunctional distribution of arable land

### Objectives

**Promoting rural development and the agricultural sector** through allowing **safe access** and development of **adequate support infrastructure**, to enable **food security and a sustainable management of the environment.**
Increasing safe access to arable land by overseeing de-mining operations and supporting the training of local experts to enable more comprehensive removal of remaining dangerous ordnance.

## Problem Area: Protecting Vulnerable Groups

### Challenges

* High population density in urban areas leads to an overabundance of informal labour
* Reduction of high share of informal labour prevalent in the general economy, affecting especially women as well as individuals in the mining sector
* Abolishment of structures of child labour
* Coping with the reintegration of social groups such as soldiers or displaced persons

### Objectives

Creating **wage employment** opportunities for especially vulnerable groups such as **youth, women, disabled persons, and displaced persons** through **sustainable use of local resources, labour-intensive infrastructural development,  vocational training** to build a system against vulnerabilities and with **social protections for those who need it most**.
**Preventing child labour, forced labour** and positions of **inadequate remuneration** based on **exploitation of status, gender etc. by appropriate application of ILO Convention 138 (Minimum age Convention) & Convention 182 (Worst forms of child labour Convention)**

# ILO - International Labour Organization for the Final Conference

## Preliminary Research

### Structure and Approach

* *founding & transformation*
  * foundation and relation to League of Nations / new multilateral order
    contrast internal projects of legitimization (esp 1920-30s) to external analysis (~70s)
    * [@Kott2018] Sandrine Kott (2018) 'Towards a social history of international organisations: the ILO and the internationalisation of western social expertise (1919-1949)' in Miguel Bandeira Jerónimo & José Pedro Monteiro (Eds.) *Internationalism, Imperialism, and the Formation of the Contemporary World: The Pasts of the Present*, : Palgrave MacMillan, pp. 33--58
  * reformation into UN institution after WWII
    papers questioning state-allegiance vs NGO status / new positioning alongside US/GB in intl networks
    * [@Kratochwil1986] F. Kratochwil and J. Ruggie, ‘‘International Organization: A State of the Art on an Art of the State’’, International Organization, 40 (1986), p. 755.
    * [@Haas1964] E. Haas, Beyond the Nation-State: Functionalism and International Organization (Stanford, CA, 1964).
    * [@Maul2012] Maul, D. (2012) *Human rights, development and decolonization: the international labour organization, 1940-70*, : Palgrave Macmillan UK, p. 86-120.
  * neoliberal change / washington consensus
    follow dwindling action potential / agency - new methods employed by actors? new alignments
    importance of US withdrawing
    * ?? [@Cox1977] R. Cox, ‘‘Labor and Hegemony’’, International Organization, 31 (1977), pp. 385–424.
* *mission & corresponding programs*
 [@Kott2013] Sandrine Kott & Joëlle Droux (2013) *Globalizing social rights: the international labour organization and beyond*, Houndmills, Basingstoke, Hampshire New York: Palgrave Macmillan UK
  * *types of projects & activities*
* *internal structure & decisionmaking*
  importance of tripartite structure & its stable nature; the primary actors/zones of conflict;
    * tripartite structure still under-analyzed, focus on trade unions almost nothing on employers [see @Daele2008, 510]
  ILO as SITE instead of actor itself
  * minutes/conference archives will be useful for discourse analysis (e.g. minutes below)
  * [@Kott2018] Sandrine Kott (2018) 'Towards a social history of international organisations: the ILO and the internationalisation of western social expertise (1919-1949)' in Miguel Bandeira Jerónimo & José Pedro Monteiro (Eds.) *Internationalism, Imperialism, and the Formation of the Contemporary World: The Pasts of the Present*, : Palgrave MacMillan, pp. 33--58
  * *members of org, key actors in staff*
    * memoirs of director general: e.g. Albert Thomas vs Bob Hawke in decision-making
* *position of the org in international network of development orgs*
  * [@Kott2016] Sandrine Kott (2016) 'Cold war internationalism' in Glenda Sluga & Patricia Clavin (Eds.) *Internationalisms: A Twentieth-Century History*, Cambridge: Cambridge University Press, pp. 340--362
  * [@Maul2012] Maul, D. (2012) *Human rights, development and decolonization: the international labour organization, 1940-70*, : Palgrave Macmillan UK

### Potential Research Designs

* recommendations and enforcement
  * The state-sponsoring of ILO and the possibility of working within specific countries
  * Money and the possibility for development efforts: Leimgruber in [@Kott2013]
* Crisis of the welfare state and ILO's internal reconfigurations during the 80s
  * [@Maul2012] Ch7 - technical assistance & north-south conflict
  * technical assistance programs - [@Maul2012, pp. 121-] transformations from 70s-80s
  * crisis of the welfare state as removing the ground to work on
* [@Maul2019] PartIV on shifting ground:
  * new leader ship, new actors and how it reflects in changing approach to 'efforts'
  * [@Maul2019] actors and their different leadership methods
  * changing tripartite structures - are the people changing that can actually change the

### Sources Todo-List

potential sources:

* ILO structure
  * [x] quick rundown - comparing it with WTO and hard/soft application of laws [@Staiger2003, 287]
  * [x] overview of ILO operating procedure and historical baggage (tripartite, anti-communism, proceduralism) [@Langille2016, 477]
  * [ ] detailed operation and law adherence, *how* things are done and in *what order* [@Servais2017]
* Angola
  * [x] Angolan (almost) post-war economy change -- analyzed mostly from above, in an effiency based manner [@Munslow1999]
  * [ ] The move towards 'democratization' of Angola questioned, as skimming of profits by elites [@Kibble2006]
  * [ ] labor patterns (manual construction) report, [@Benach2007]
  * [ ] WHO labor landscape Angola report [@Oya2019]
* ILO operations in the 80s
  * [ ] operation (and issues) in Argentine around same time [@Daele2010, 401ff, 423ff]
  * [ ] operation supporting Solidarity Movement in Poland ~1980s [@Standing2008]

### ILO Basic Information
* formed in 1919, one of (if not *the* oldest continuously operating IO)
* headquartered in Gevena, offices (nowadays) in >40 countries
* generally, reliance of soft law and persuasive measures for generating action (standards)
  * few binding instruments, unlike e.g. WTO's dispute settlements [@Maupain2013]
* ideas:
  * founded on principle of 'social dialogue between social partners in labor regulation', committing to the contestability of issues in labor
  * in favor of regulation as principle of satisfying contestability (historically as instrument of bulwark against radical worker mobilization, i.e. Communist threat 1919)
  * "an agency 'created of the capitalist system,' rather than an agency 'for the transformation of capitalism'", representing 'embedded liberalism' strain of development to counter-act Soviet model [see @Standing2008]
* structure:
  * tripartite structure: three-way split between state governments and 2 nongovernmental representatives: workers and employers
  * voting and consensus is handled on a 2:1:1, so 50% state actors, 50% non-state actors
  * non-state representatives are elected by the respective member states' unions and employer organizations
* procedure: [see @Langille2016, 477]
  * annual conferences (international convening parliament); out of which 'international labor standards' derive; which together create (by now ~200) international treaties called Conventions
  * when convention is ratified by a member state, it becomes 'binding' ILO law; and is usually (but not always!) adopted into domestic law
  * ILO contains a 'supervisory system' which reports on individual member states' adoption and application of the standards, supervises any complaints, and provides *support* [ILO Supervisory System/Mechanisms on ILO.org](https://www.ilo.org/global/about-the-ilo/how-the-ilo-works/ilo-supervisory-system-mechanism/lang--en/index.htm)
  * support consists of: technical assistance, providing labor training / administrative training and resources; assistance in drafting domestic laws
* challenges:
  * especially 1989 thus watershed moment for ILO, opened 'contestability' debate further (with opposition to Communism now removed), and firmly embedded market primacy into global economy; infusing many standard-setting procedures with new incoherence [@Langille2016, 479]
  * 'contestability' is *still* issue of stand-still, often re-framed as avoiding 'race to the bottom' dilemma in major debates
  * persuasion vs coercion: the question of how 'hard' labor standards violations should be punished is still not conclusively decided
  * tripartite crisis: especially in 50s, 70s, and from 2000s, often claimed as ineffective (or, variously, non-existent) functioning of tripartite; with ILO falling back into inter-governmental agency voting 'over' non governmental actors
  * missing 'narrative': with adoption of 1998 Declaration, local context is put over universal legal rules; following arguments often argue over need of new overarching narrative of ILO [@Langille2016, 481]

### Internal Structure of the ILO

from [@Staiger2003, 287ff.]

* basic credo: "to improve the working and living conditions of workers everywhere" [@Morse1969, 96]
  * "social justice", contained in ILO Constitution Preamble symbolize its main objective [@Johnston1970, 13]
* but disagreement over means: [@Morse1969, 82-83]
  * standard-setting organization to defend rights of workers against exploitation in drive for growth, development, and industrialization
  * operational organization, which should concentrate efforts on promoting economic development
  * forum for tripartite discussion
  * assisting in training of labor force
  * unofficial, since conception, competitive effects on markets are also on mind of ILO creation [see @Staiger2003, 288]
* [@Johnston1970, 88] sees 3 main purposes:
  * standard setting
  * information dissemination
  * technical assistance
* tripartite structure:
  * basic explanation: [@Langille2016, 477-483]
  * procedure explanation: [@Servais2017]
  * Jeffrey Harrod ciritical assessment in ILO:Past and Present conference 2007
  * Alcock 196ff explains the structure and its difficulty in adapting to post-war climate
  * creation of regional, field and liaison offices 1969 shortly explained [@Alcock1979, 240]
  * crisis of tripartite structure ~1955 [@Alcock1979, 300-305]
  * [@Maul2012, 278], new problems for the tripartite structure 60s+

### ILO and Angola

#### General Situation 2003, after (actual) end Civil War

* situation in Angola
  * 2003: no access to drinking water (60%), medical care (80%), high child death rate (30%), 100.000 children separated from parents
  * rural flight into urban locations
  * remaining landmines (and no government provisions for their casualties / victims); demining operations by HALO Trust, with help form foreign experts
  * *huge* amount of (internally) displaced persons
  * child soldiers and child marriages, UNITA esp. but not exclusively
* phrasing of position
  * TODO, see [@Standing2008] souruces?

#### Process toward End of War

* 22 Feb 2003, KIA of UNITA leader Jonas Savimbi (and directly succeeding leader 20 days later)
* unveiling of 15-point peace agenda by Angolan Armed Forces 13 March
  * called for cessation of all military offensives by FAA at midnight
  * ad-hoc meetings of field commanders to institute ceasefire
  * de-militarization and *reintegration* of UNITA troops
  * general amnesty for troops, to further reconciliation
* Memorandum of Understanding 4 April
  * not complete peace accord yet, but initiation of peace process and implementation of remaining issues from Lusaka Protocol
* technical group composed of FAA and UNITA members lead quartering and demilitarization by 15 Jun
  * quartering into 36 quartering areas for ~85,000 UNITA military personnel and ~240,000 family members
  * gov pledged reintegration:
    > "The international community will need to help with the integration of former combatants, including support for vocational training, employment-generation activities and reconciliation programmes. Apart from technical and managerial expertise and advice, the United Nations can offer its good offices to help prevent or resolve conflicts that may arise." [@UNSCRep2002, 3]
    > "[international organizations] have agreed on a common approach aimed at stabilizing the conditions of the most acutely affected populations, while simulateneously undertaking community-based initiatives to facilitate the return, resettlement and reintegration of displaced populations." [@UNSCRep2002, 6]

#### Labor conditions 1950-2000

* commodified labor force 1960 92percent male, women populating reciprocal sphere [@Vos2014, 380]
  > husbands and sonds, working as independent farmers or as paid employees of European planters and businessmen, were officially recognized as income-earners, whereas the labor of their wives and daughters was generally considered as a non-remunerated contribution to the maintenance of the household.
* from 2000 collapse of formal economy, thus wage labor; formal sector employment *only* increased within state (i.e. public administration, police, armed forces, state-owned companies)
* private sector rapid expansion of informal economy (esp urban due to displacement): "small businessmen employing small numbers of wage laborers and [...] self-employed workers, mostly trying to scrape an income together from petty trade." [@Vos2014, 381]
* oil revenues hinder commercial agriculture, many peasants thrown back to subsistence farming; distribution of "large plots of fertile land [...] to clients of the government, constituting a new class of big landowners (*fazendeiros*)" [@Vos2014, 381]
* largely support through informal economy, even public servants often used private sector activities to supplement dwindling real wages; same procedure "stimulated the self-employment of women in the informal sector." [Vos2014, 382]
* great degree of informality in (esp Chinese sub-contractor) low-skill construction worker & factory sites (16% with contract, 23% semi-skilled with contract); *but* most were informed verbally of wage expectations, working hours, leave policy, benefits [@Oya2019, 37]
* however, combination of (low but stable) cash wage and 'social wage' (living in accommodations by Chinese firm and food supplied) allowed some saving of money and intermittent escape from poverty trap [@Oya2019, 43-44]
* unionization, training, skill development ?? [@Oya2019, 47-50]

## Position Paper

Structure

* Preamble
  * problems of the country, reflected by "major sources of insecurity and anxiety, the answers are: civil disorder, armed violence, instability of governance system too often caused by unemployment, inequalities, poverty, discrimination and lack of participation."
  * these things [verändert für uns] led the people of these countries into too many years of sufferung from such sources of individual and collective insecurity.
  * The people of Angola do not deserve such continued conditions of deprivation and struggle, for they are a region blessed with an abundance of [human resources] and [other positive aspects]
  * Indeed, through thinking and acting collectively and in unison, we can address and overcome the pressing challenges facing the country of Angola at this moment into a future of stability and prosperity.
* Concrete Struggles
  * little access to drinking water, medicine, basic human needs being seen to -> [other agencies] we all need to see to the immediate improvement of this situation
* immediate objectives
  * creating wage employment opportunities for youth, for women, and displaced persons through [equality] use of local resources, labour-intensive infrastructural development to [erschaffen] a a system against vulnerabilities and with social protections for those who need it most
  * enabling social dialogue for labor through reintegration of erstwhile combatants who became displaced in the struggle, through vocational training, targeted training and administrative assistance, as well as an emphasis on strengthening unionizing possibilities for the [common worker]
  * preventing child labor, forced labor and positions of inadequate remuneration based on exploitation of status, gender or other fundamental []

* wage labor / better labor remuneration
  - Informal Labour mainly women
  - 1960s: 92% men waged labour - continuously going down over time
* Agrar
  - Private sector: agrar zusammengebrochen,
  * because most money exported through oil and diamonds
  - Land mines complicate work on fields --> famine, poverty
  - Cities overpopulated - high unemployment and informal labour
- Child labour (study on application of ILO conventions against child labour (portuguese)
  - Forced labour in diamond
- 2005: land grabbing (distribution of arable land to political and economic elites)
- Deep in 2000s: not many unions existing
- Reintegration of soldiers, vocational training
* Displaced persons:

## Resources

* [internal document server](https://www.ilo.org/wcmsp5/idcplg?IdcService=GET_DOC_PAGE&Action=GetTemplatePage&Page=HOME_PAGE)
* [ILO Minutes meeting 336](https://www.ilo.org/wcmsp5/groups/public/---ed_norm/---relconf/documents/meetingdocument/wcms_713459.pdf)
* [ILO Labordoc - Reports on Programme, budget and possibility & Recommendations and Enforcement & Director-Generals](https://libguides.ilo.org/documentation/ILC)
  * highlights diverging aims of different tripartite actors; & methods


# Related

[Meeting Draft 2019-11-14](1911220955 ILO Meeting Draft 2019-11-14.md)
[Documents on Angola](1912161703 ILO Documents on Angola.md)

# References

[Van Daele - The ILO in Past and Present Research](1911201337 Van Daele - The ILO in Past and Present Research.md)

[Kott - The ILO and the Internationalisation of Western Social Expertise](1911220910 Kott - The ILO and the Internationalisation of Western Social Expertise.md)

# ILO - International Labour Organization for the Final Conference

## Structure

* *founding & transformation*
  * foundation and relation to League of Nations / new multilateral order
    contrast internal projects of legitimization (esp 1920-30s) to external analysis (~70s)
    * [@Kott2018] Sandrine Kott (2018) 'Towards a social history of international organisations: the ILO and the internationalisation of western social expertise (1919-1949)' in Miguel Bandeira Jerónimo & José Pedro Monteiro (Eds.) *Internationalism, Imperialism, and the Formation of the Contemporary World: The Pasts of the Present*, : Palgrave MacMillan, pp. 33--58
  * reformation into UN institution after WWII
    papers questioning state-allegiance vs NGO status / new positioning alongside US/GB in intl networks
    * [@Kratochwil1986] F. Kratochwil and J. Ruggie, ‘‘International Organization: A State of the Art on an Art of the State’’, International Organization, 40 (1986), p. 755.
    * [@Haas1964] E. Haas, Beyond the Nation-State: Functionalism and International Organization (Stanford, CA, 1964).
    * [@Maul2012] Maul, D. (2012) *Human rights, development and decolonization: the international labour organization, 1940-70*, : Palgrave Macmillan UK, p. 86-120.
  * neoliberal change / washington consensus
    follow dwindling action potential / agency - new methods employed by actors? new alignments
    importance of US withdrawing
    * ?? [@Cox1977] R. Cox, ‘‘Labor and Hegemony’’, International Organization, 31 (1977), pp. 385–424.
* *mission & corresponding programs*
 [@Kott2013] Sandrine Kott & Joëlle Droux (2013) *Globalizing social rights: the international labour organization and beyond*, Houndmills, Basingstoke, Hampshire New York: Palgrave Macmillan UK
  * *types of projects & activities*
* *internal structure & decisionmaking*
  importance of tripartite structure & its stable nature; the primary actors/zones of conflict;
  ilo as SITE instead of actor itself
  * minutes/conference archives will be useful for discourse analysis (e.g. minutes below)
  * [@Kott2018] Sandrine Kott (2018) 'Towards a social history of international organisations: the ILO and the internationalisation of western social expertise (1919-1949)' in Miguel Bandeira Jerónimo & José Pedro Monteiro (Eds.) *Internationalism, Imperialism, and the Formation of the Contemporary World: The Pasts of the Present*, : Palgrave MacMillan, pp. 33--58
  * *members of org, key actors in staff*
    * memoirs of director general: e.g. Albert Thomas vs Bob Hawke in decision-making
* *position of the org in international network of development orgs*
  * [@Kott2016] Sandrine Kott (2016) 'Cold war internationalism' in Glenda Sluga & Patricia Clavin (Eds.) *Internationalisms: A Twentieth-Century History*, Cambridge: Cambridge University Press, pp. 340--362
  * [@Maul2012] Maul, D. (2012) *Human rights, development and decolonization: the international labour organization, 1940-70*, : Palgrave Macmillan UK

## Research Design

* recommendations and enforcement
  * The state-sponsoring of ILO and the possibility of working within specific countries
  * Money and the possibility for development efforts: Leimgruber in [@Kott2013]
* Crisis of the welfare state and ILO's internal reconfigurations during the 80s
  * [@Maul2012] Ch7 - technical assistance & north-south conflict
  * technical assistance programs - [@Maul2012, pp. 121-] transformations from 70s-80s
  * crisis of the welfare state as removing the ground to work on
* [@Maul2019] PartIV on shifting ground:
  * new leader ship, new actors and how it reflects in changing approach to 'efforts'
  * [@Maul2019] actors and their different leadership methods
  * changing tripartite structures - are the people changing that can actually change the

## Resources

* [internal document server](https://www.ilo.org/wcmsp5/idcplg?IdcService=GET_DOC_PAGE&Action=GetTemplatePage&Page=HOME_PAGE)
* [ILO Minutes meeting 336](https://www.ilo.org/wcmsp5/groups/public/---ed_norm/---relconf/documents/meetingdocument/wcms_713459.pdf)
* [ILO Labordoc - Reports on Programme, budget and possibility & Recommendations and Enforcement & Director-Generals](https://libguides.ilo.org/documentation/ILC)
  * highlights diverging aims of different tripartite actors; & methods


# Related

[Meeting Draft 2019-11-14](1911220955 ILO Meeting Draft 2019-11-14.md)

# References

[Van Daele - The ILO in Past and Present Research](1911201337 Van Daele - The ILO in Past and Present Research.md)

[Kott - The ILO and the Internationalisation of Western Social Expertise](1911220910 Kott - The ILO and the Internationalisation of Western Social Expertise.md)

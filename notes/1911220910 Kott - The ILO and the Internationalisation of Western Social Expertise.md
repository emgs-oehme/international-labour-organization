# Kott - The ILO and the Internationalisation of Western Social Expertise

* argues for international organizations as "not so much *actors* in global governance as they are *sites* of internationalisation" (34)
* argues for using archival documents instead of official publications (35)
* 4 points: (35)
  * analyze ILO as site of coalescing NGO liberal West European networks
  * national/international dialectic of developing intl expertise
  * 'brown internationalism' and its conflicts with liberal internationalism
  * export and re-appropriation of liberal social normativity in peripheral spaces

sources:

* esp idea culture in institutions: Jasmien Van Daele, “Engineering Social Peace: Networks, Ideas, and the Founding of the International Labour Organization”, International 52 S. Kott Review for Social History 50 (2005), pp. 435–466; Madeleine Herren, Internationale Sozialpolitik vor dem Ersten Weltkrieg. Die Anfänge europäischer Kooperation aus der Sicht Frankreichs (Berlin: Duncker & Humboldt, 1993); Sandrine Kott, “From Transnational Reformist Network to International Organization: the International Association for Labour Legislation and the International Labour Organization (1900–1930s)”, in Shaping the Transnational Sphere, eds. Davide Rodogno, Bernhard Struck, and Jakob Vogel (New York: Berghahn Books, 2014), pp. 383–418.

# Related

# References

# Van Daele - The ILO in Past and Present Research

## Why study the ILO

* 'trendsetter' among international organizations (485)
  * in standard-setting (by means of conventions/recommendations)
  * in technical cooperation and international expertise on labor
* unique tripartite structure (485-86)
  * representatives from government, employers, trade unions
  * seems stable in that it remained unchanged to today
* *oldest* international organization
  * founded in 1919 as part of League of Nations
  * only institution that has unbroken history since then

* analysis of ILO shifts with 3 surrounding transformations (486)
  * transformations within the ILO as an institution
  * transformations in intl political, economic, social context
  * transformations in the scientific disciplines (es. labor history, intl relations)
* from 2 angles (487)
  * 'inside' - by IL Office, Secretariat in Geneva, (former) ILO officials, often to justify its work
  * 'outside' - by academics in independent scientific position

* general hypothesis (486-87)
  * interest in international organizations is related to general importance placed on multilateral organizations and belief of effectiveness of international cooperation
  * aksing how 'global' research on ILO has been

## Literature overview: 1920s legitimization project

* early literature much more punctuated by 'inside' perspective - first-generation products of 1920s (488-89)
  * esp geared towards ILO director Albert Thomas
  * from international law / political science perspective in broader framework of international relations
  * outside literature still closely aligned to inside perspectives
  * also generally optimistic view, overview of policy methods etc (490)
  * EU centric, with understanding of 'club of like-minded states'; exception being India actively participating in orga (491)
  * "historical writing in the 1920s can be defined as an instrument of self-justification", as a legitimizing concept for the organization (491)

sources;

* self-legitimization: A. Thomas, ‘‘The International Labour Organization: Its Origins, Development and Future’’, International Labour Review [hereafter ILR], 1 (1921); this article was reprinted in ILR, 135 (1996), pp. 261–276, 263.
* connection with League of NAtions: George Nicoll Barnes (1859–1940), former trade-union Secretary, member of the Labour Party, and leader of the British delegation at the Paris Peace Conference in 1919; G.N. Barnes, History of the International Labour Office (London, 1926). See also his autobiography: idem, From Workshop to War Cabinet (London, 1923).
* ILO and eurocentrism (its self-understanding): K.N. Dahl, ‘‘The Role of ILO Standards Policy in the Global Integration Process’’, Journal of Peace Research, 5 (1968), p. 321.

## 1930s- 45

* generally continuing on from previous decade but different international context: mass unemployment, nationalization, economic crisis (492)
* fewer ratifications of ILO conventions; writings in defense of ILO (492)
* Albert Thomas death and biographical literature in same (defensive) vein (492)
* shift of ILO from dealing "with the social effects rather than with the causes of existing economic conditions" (493)
  * begins actively advocating "measures of monetary and credit policy, international trade, and public works" (493)
* debates center around state-connections and autonomy of ILO (esp after entry of US into ILO 1934)
  * much literature in front of New Deal measures of US
* in/after WWII, while analyses remained institutional, taking stock of past achievements & challenges major theme (495)
  * repositioning alongside US and GB to reconfirm its new UN umbrella

sources:

* questioning law-centricity and state-closeness from AFL: M. Woll, ‘‘The International Labour Office: A Criticism’’, Current History, 31 (1930), pp. 683–689. Matthew Woll was vice-president of the AFL.
* Shotwell with source-rich overview of funding of ILO: J.T. Shotwell, The Origins of the International Labour Organization (New York, 1934), 2 vols, pp. xxix–xxx.

## (late) 1940s - (mid) 1970s

* topics stayed on (internal) literature of 'self-identity', now in context of postwar international relations (496)
  * popular topics also how ILO remained in existence after WWII, and how its internal structures evolved
* Nobel Peace Price for 50th anniversary lead to explosion of historical reviews (496)
  * this is where Alcock was commissioned for a history (chronological, embedded in international historical context, as opposed to mostly internal institutional)
* 1950s and esp 1960s start academic review (critique/analysis) of ILO (497-98)
  * professionalization of the field - esp in historiography
  * from institutional aspects to actual decision=making process within ILO - "opening the black box"
  * esp Cox and Haas with major empirical research (499)
* increasing discrepancy between original constitutional design and daily practice becomes focus of analyses (500)

sources:

*  analysis of patterns shaping ILO outcomes: F. Kratochwil and J. Ruggie, ‘‘International Organization: A State of the Art on an Art of the State’’, International Organization, 40 (1986), p. 755.
* analysis of supervision of international standards: E.A. Landy, The Effectiveness of International Supervision: Thirty Years of ILO Experience (London [etc.], 1966); idem, ‘‘The Effectiveness of International Labour Standards: Possibilities and Performance’’, ILR, 101 (1970), pp. 555–604.
* analysis of tripartite structure:  T. Landelius, Workers, Employers and Governments: A Comparative Study of Delegations and Groups at the International Labour Conference, 1919–1964 (Stockholm, 1965); A. Suviranta, The Role of the Member State in the Unification Work of the International Labour Organization (Helsinki, 1966).
* **ILO as institutionalized interest politics**:  E. Haas, Beyond the Nation-State: Functionalism and International Organization (Stanford, CA, 1964).
* ILO as its own political system: R. Cox, ‘‘ILO: Limited Monarchy’’, in R. Cox, H. Jacobson, and G. Curzon (eds), The Anatomy of Influence: Decision Making in International Organization (New Haven, CT [etc.], 1973), pp. 102–138.

## 1970s - 1980s

* politicization and less autonomy of ILO due to cold war rifts (external) & quick changing Directors-General (internal) (502)
  * withdrawal of US from ILO over condemnation of Israel & admittance of PLO -> budget cuts (502)
* neo-liberal Washington consensus turn further immobilizes ILO (502)
  * can't play active role due to members' political divisions
  * liberalization & deregulation let social dialogue & social security/welfare take a backseat

sources:

* gramscian approach to intl-orgs as hegemonic power vehicle: R. Cox, ‘‘Labor and Hegemony’’, International Organization, 31 (1977), pp. 385–424.

## 1990s -

* renewed interest due to
  * nation-states looking to participate in multilateral structures (504)
  * changes in international society/global political order renewed role of international orgs (504)
* new approaches: constructivist gender, decolonization, human rights, indigenous/forced labor, epistemic communities, intellectuals/international expertise, social security, construction of welfare regimes, child labor, transnational NGO networks, ..

sources:

* historical perspective on new 'global' meaning of ILO: M. Nieves Roldan-Confesor, ‘‘Labour Relations and the ILO Core Labour Standards’’, in M. van der Linden and T. Koh (eds), Labour Relations in Asia and Europe (Singapore, 2000), pp. 19–52.
* multitude of sources 504-505 - different dimensions, no clear guide (article itself from '08)
* e.g. S. Kott, ‘‘Arbeit. Ein transnationales Objekt? Die Frage der Zwangsarbeit im ‘Jahrzehnt der Menschenrechte’’’, in C. Benninghaus et al. (eds), Unterwegs in Europa. Beiträge zu einer pluralen europäischen Geschichte (Frankfurt, 2008, forthcoming).


# Related

# References

Jasmien Van Daele (2008) 'The international labour organization (ILO) in past and present research', *International Review of Social History* 53, 485--511
